'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var path = require('path');
var readline = require('readline');
var fs = require('fs');
var mdPreKroki = require('@dgwnu/md-pre-kroki');

/**
 * File Utilities for generic purposes
 */
/**
* Resolve the default or custom template path
* @param defaultPath default template path (required and __dirname in most cases)
* @param templatePath custom templatre path (optional)
* @returns the resolved template path
*/
function resolveTemplatePath(defaultPath, templatePath) {
    var resolvedTemplatePath = '';
    if (!templatePath) {
        resolvedTemplatePath = path.resolve(defaultPath, '..', '..', 'template');
    }
    else {
        resolvedTemplatePath = path.resolve(templatePath);
    }
    return resolvedTemplatePath;
}
/**
 * Extract the file name from a file path
 * @param filePath
 * @returns file name
 */
function extractFileName(filePath) {
    var parts = filePath.split(path.sep);
    return parts[parts.length - 1];
}
/**
 * Chunk input text file by seperator string into one or more output files
 * @param inputFilePath location input text file
 * @param lineSeperator line seperator value
 * @param outputPath location seperated output files path
 */
function chunkTxtFile(inputFilePath, lineSeperator, outputPath) {
    var chunckFileEnd = extractFileName(inputFilePath);
    var chunckFileNr = 1;
    var chunckFilePath = initAppendFilePath(outputPath, chunckFileNr, chunckFileEnd);
    console.log("chunckFilePath ==> " + chunckFilePath);
    var firstLine = true;
    var rl = readline.createInterface({
        input: fs.createReadStream(inputFilePath),
        output: process.stdout,
        terminal: false
    });
    rl.on('line', function (line) {
        if (!firstLine && line.startsWith(lineSeperator)) {
            // Next file to chunk
            chunckFileNr++;
            chunckFilePath = initAppendFilePath(outputPath, chunckFileNr, chunckFileEnd);
            console.log("chunckFilePath ==> " + chunckFilePath);
        }
        // append line to current file chunk
        fs.appendFileSync(chunckFilePath, line + '\n');
        if (firstLine) {
            // passed first line so next line seperator is next file to chunk
            firstLine = false;
        }
    });
}
/**
 * Initialize file befote append to it be removing existing file and return resolved path
 * @param outputPath
 * @param outputFileNr
 * @param outputFileEnd
 * @returns resolved path to append file
 */
function initAppendFilePath(outputPath, outputFileNr, outputFileEnd) {
    var appendFilePath = path.resolve(outputPath, outputFileNr + "_" + outputFileEnd);
    if (fs.existsSync(appendFilePath)) {
        // remove existing file before append to file
        fs.unlinkSync(appendFilePath);
    }
    return appendFilePath;
}
/**
 * Create destination directory (if not exist already) and copy relative source file
 * to destination directory (creates subdirectories in destiation directory based on
 * relative source file path)
 *
 * @param relFilePath relative source file path
 * @param destDir destination directory
 */
function relativeCopyFile(relFilePath, destDir) {
    // create destination path
    mdPreKroki.createSubDirectories(destDir, relFilePath);
    // copy file to destinations path
    fs.copyFileSync(relFilePath, path.resolve(destDir, relFilePath));
}
/**
 * Copy files from one folder to another folder (and create subdirectories if they don't exist)
 * @param pathFrom Folder to copy files from
 * @param pathTo Folder to copy files to
 */
function copyFolder(pathFrom, pathTo) {
    // create subdirectories in path to (if not exists)
    mdPreKroki.createSubDirectories('.', path.resolve(pathTo, 'file.stub'));
    fs.readdirSync(pathFrom).forEach(function (fileToCopy) {
        fs.copyFileSync(path.resolve(pathFrom, fileToCopy), path.resolve(pathTo, fileToCopy));
    });
}
/**
 * Remove folder with all contents of subfolders and files (if exists)
 *
 * @param folderPath Path to folder
 */
function removeFolder(folderPath) {
    var files = [];
    var absPath = path.resolve(folderPath);
    if (fs.existsSync(absPath)) {
        files = fs.readdirSync(absPath);
        for (var _i = 0, files_1 = files; _i < files_1.length; _i++) {
            var file = files_1[_i];
            var curPath = path.resolve(absPath, file);
            if (fs.lstatSync(curPath).isDirectory()) {
                // recurse
                removeFolder(curPath);
            }
            else {
                // delete file
                fs.unlinkSync(curPath);
            }
        }
        // delete folder after all contents is deleted
        fs.rmdirSync(absPath);
    }
}

/**
 * Generic Markdown related functions
 */
// constants
var VALID_IMG_REFS = ['./', '/', 'https://', 'http://'];
/**
 * Create a template to parse variable into
 * @param templateVar name of the variable to parse into the template (required)
 * @param startMark start marking string of the template (optional)
 * @param endMark end marking string of the template (optional)
 * @returns template string to parse variable into
 */
function createTemplate(templateVar, startMark, endMark) {
    if (startMark === void 0) { startMark = '<%='; }
    if (endMark === void 0) { endMark = '%>'; }
    return startMark + " " + templateVar + " " + endMark;
}
/**
 * Get first line (Header)
 * @param mdContentStr
 * @returns Md Header without spaces
 */
function getMdHeaderStr(mdContentStr) {
    return mdContentStr.split('\n')[0].substring(1).trim();
}
/**
 * Get all sections (Markdown HEADER LEVEL 2)
 * @param summaryMdStr
 * @returns sections array
 */
function getSummarySections(summaryMdStr) {
    var summarySections = [];
    for (var _i = 0, _a = summaryMdStr.split('\n').filter(function (mdLine) { return mdLine.trimStart().startsWith('##') || mdLine.trimStart().startsWith('*'); }); _i < _a.length; _i++) {
        var mdLine = _a[_i];
        if (mdLine.startsWith('##')) {
            // start of a new section
            summarySections.push({ title: mdLine.split('##')[1].trim(), summaryLines: [] });
        }
        else {
            // summary line of current section
            summarySections[summarySections.length - 1].summaryLines.push(mdLine);
        }
    }
    return summarySections;
}
/**
 * Get summary lines of a section content string
 * @param summarySectionMdStr
 * @returns summary lines array
 */
function getSummaryLines(summarySectionMdStr) {
    return summarySectionMdStr.split('\n').filter(function (mdLine) { return mdLine.trimStart().startsWith('*'); });
}
/**
 * Get summary line section id
 * @param summaryLine
 * @returns id name string
 */
function extractSectionId(summaryLine) {
    return extractData(summaryLine, '[', ']').toLowerCase();
}
/**
 * Get summary line link data include
 * @param summaryLine
 * @returns link string
 */
function extractDataInclude(summaryLine) {
    return extractData(summaryLine, '(', ')');
}
/**
 * Extract content between start and end id
 * @param content
 * @param startId
 * @param endId
 * @returns extracted string
 */
function extractData(content, startId, endId) {
    return content.split(startId)[1].split(endId)[0];
}
/**
 * Parse Image as Markdown Image Reference
 * @param imageId id or name of the image
 * @param imageRef link to the image
 * @returns Markdown Image Reference string
 */
function parseMdImage(imageId, imageRef) {
    // fix relative path reference (to use in VuePress and does not harm other applications)
    var preRef = './';
    if (VALID_IMG_REFS.find(function (validImgRef) { return imageRef.startsWith(validImgRef); })) {
        // there is already a valid reference
        preRef = '';
    }
    return "![" + imageId + "](" + preRef + imageRef + ")";
}
/**
 * Get markdown link reference from content
 * @param mdContent content string
 * @returns reference of the link (relative or URL)
 */
function getMdContentLinkRef(mdContent) {
    return extractData(mdContent, '(', ')');
}
/**
 * Get markdown link name from content
 * @param mdContent content string
 * @returns name of the link
 */
function getMdContentLinkName(mdContent) {
    return extractData(mdContent, '[', ']');
}

exports.chunkTxtFile = chunkTxtFile;
exports.copyFolder = copyFolder;
exports.createTemplate = createTemplate;
exports.extractData = extractData;
exports.extractDataInclude = extractDataInclude;
exports.extractFileName = extractFileName;
exports.extractSectionId = extractSectionId;
exports.getMdContentLinkName = getMdContentLinkName;
exports.getMdContentLinkRef = getMdContentLinkRef;
exports.getMdHeaderStr = getMdHeaderStr;
exports.getSummaryLines = getSummaryLines;
exports.getSummarySections = getSummarySections;
exports.initAppendFilePath = initAppendFilePath;
exports.parseMdImage = parseMdImage;
exports.relativeCopyFile = relativeCopyFile;
exports.removeFolder = removeFolder;
exports.resolveTemplatePath = resolveTemplatePath;
