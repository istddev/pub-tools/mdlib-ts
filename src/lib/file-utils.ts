/**
 * File Utilities for generic purposes
 */

/**
 * Node Package Imports
 */
 import { resolve, sep } from 'path';
 import { createInterface } from 'readline';
 import { appendFileSync, createReadStream, existsSync, unlinkSync, copyFileSync, 
     readdirSync, rmdirSync, lstatSync } from 'fs';
 
 import { createSubDirectories } from '@dgwnu/md-pre-kroki';

 /**
 * Resolve the default or custom template path
 * @param defaultPath default template path (required and __dirname in most cases)
 * @param templatePath custom templatre path (optional)
 * @returns the resolved template path 
 */
export function resolveTemplatePath(defaultPath: string, templatePath?: string) {
    let resolvedTemplatePath = '';

    if (!templatePath) {
        resolvedTemplatePath = resolve(defaultPath, '..', '..', 'template');
    } else {
        resolvedTemplatePath = resolve(templatePath);
    }

    return resolvedTemplatePath;
}

/**
 * Extract the file name from a file path
 * @param filePath 
 * @returns file name
 */
 export function extractFileName(filePath: string) {
    const parts = filePath.split(sep);
    return parts[parts.length - 1];
}

/**
 * Chunk input text file by seperator string into one or more output files
 * @param inputFilePath location input text file
 * @param lineSeperator line seperator value
 * @param outputPath location seperated output files path
 */
export function chunkTxtFile(inputFilePath: string, lineSeperator: string, outputPath: string) {
    const chunckFileEnd = extractFileName(inputFilePath);
    let chunckFileNr = 1;
    let chunckFilePath = initAppendFilePath(outputPath, chunckFileNr, chunckFileEnd);
    console.log(`chunckFilePath ==> ${chunckFilePath}`);
    let firstLine = true;

    const rl = createInterface({
        input: createReadStream(inputFilePath),
        output: process.stdout,
        terminal: false
    });

    rl.on('line', (line: string) => {

        if (!firstLine && line.startsWith(lineSeperator)) {
            // Next file to chunk
            chunckFileNr ++;
            chunckFilePath = initAppendFilePath(outputPath, chunckFileNr, chunckFileEnd);
            console.log(`chunckFilePath ==> ${chunckFilePath}`);
        }

        // append line to current file chunk
        appendFileSync(chunckFilePath, line + '\n');

        if (firstLine) {
            // passed first line so next line seperator is next file to chunk
            firstLine = false;
        }

    });

}

/**
 * Initialize file befote append to it be removing existing file and return resolved path
 * @param outputPath 
 * @param outputFileNr 
 * @param outputFileEnd 
 * @returns resolved path to append file
 */
export function initAppendFilePath(outputPath: string, outputFileNr: number, outputFileEnd: string) {
    const appendFilePath = resolve(outputPath, `${outputFileNr}_${outputFileEnd}`);

    if (existsSync(appendFilePath)) {
        // remove existing file before append to file
        unlinkSync(appendFilePath);
    }

    return appendFilePath;
}

/**
 * Create destination directory (if not exist already) and copy relative source file 
 * to destination directory (creates subdirectories in destiation directory based on 
 * relative source file path)
 * 
 * @param relFilePath relative source file path
 * @param destDir destination directory
 */
export function relativeCopyFile(relFilePath: string, destDir: string) {
    // create destination path
    createSubDirectories(destDir, relFilePath);
    // copy file to destinations path
    copyFileSync(relFilePath, resolve(destDir, relFilePath));
}

/**
 * Copy files from one folder to another folder (and create subdirectories if they don't exist)
 * @param pathFrom Folder to copy files from
 * @param pathTo Folder to copy files to
 */
export function copyFolder(pathFrom: string, pathTo: string) {
    // create subdirectories in path to (if not exists)
    createSubDirectories('.', resolve(pathTo, 'file.stub'));

    readdirSync(pathFrom).forEach(fileToCopy => {
        copyFileSync(resolve(pathFrom, fileToCopy), resolve(pathTo, fileToCopy));
    });

}

/**
 * Remove folder with all contents of subfolders and files (if exists)
 * 
 * @param folderPath Path to folder
 */
export function removeFolder(folderPath: string) {
    let files: string[] = [];
    const absPath = resolve(folderPath);

    if (existsSync(absPath)) {
        files = readdirSync(absPath);

        for (const file of files) {
            const curPath = resolve(absPath, file);

            if(lstatSync(curPath).isDirectory()) { 
                // recurse
                removeFolder(curPath);
            } else { 
                // delete file
                unlinkSync(curPath);
            }

        };

        // delete folder after all contents is deleted
        rmdirSync(absPath);
    }

};