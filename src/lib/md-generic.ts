/**
 * Generic Markdown related functions
 */

/**
 * Node Package Imports
 */

// interfaces
/**
 * Summary Content Line Section Data Interface
 */
export interface SummarySection {
    title: string; 
    summaryLines: string[];
}

// constants
const VALID_IMG_REFS = ['./', '/', 'https://', 'http://'];

/**
 * Create a template to parse variable into
 * @param templateVar name of the variable to parse into the template (required)
 * @param startMark start marking string of the template (optional)
 * @param endMark end marking string of the template (optional)
 * @returns template string to parse variable into
 */
 export function createTemplate(templateVar: string, startMark: string = '<%=', endMark: string = '%>') {
    return `${startMark} ${templateVar} ${endMark}`;
}

/**
 * Get first line (Header)
 * @param mdContentStr 
 * @returns Md Header without spaces
 */
export function getMdHeaderStr(mdContentStr: string) {
    return mdContentStr.split('\n')[0].substring(1).trim();
}

/**
 * Get all sections (Markdown HEADER LEVEL 2)
 * @param summaryMdStr 
 * @returns sections array
 */
export function getSummarySections(summaryMdStr: string) {
    let summarySections: SummarySection[] = [];

    for (const mdLine of summaryMdStr.split('\n').filter(mdLine => mdLine.trimStart().startsWith('##') || mdLine.trimStart().startsWith('*'))) {
        if (mdLine.startsWith('##')) {
            // start of a new section
            summarySections.push({ title: mdLine.split('##')[1].trim(), summaryLines: [] });
        } else {
            // summary line of current section
            summarySections[summarySections.length - 1].summaryLines.push(mdLine);
        }
    }

    return summarySections;
}

/**
 * Get summary lines of a section content string
 * @param summarySectionMdStr 
 * @returns summary lines array
 */
export function getSummaryLines(summarySectionMdStr: string) {
    return summarySectionMdStr.split('\n').filter(mdLine => mdLine.trimStart().startsWith('*'));
}

/**
 * Get summary line section id
 * @param summaryLine 
 * @returns id name string
 */
export function extractSectionId(summaryLine: string) {
    return extractData(summaryLine, '[', ']').toLowerCase();
}

/**
 * Get summary line link data include
 * @param summaryLine 
 * @returns link string
 */
export function extractDataInclude(summaryLine: string) {
    return extractData(summaryLine, '(', ')');
}

/**
 * Extract content between start and end id
 * @param content 
 * @param startId 
 * @param endId 
 * @returns extracted string
 */
export function extractData(content: string, startId: string, endId: string) {
    return content.split(startId)[1].split(endId)[0];
}

/**
 * Parse Image as Markdown Image Reference
 * @param imageId id or name of the image
 * @param imageRef link to the image 
 * @returns Markdown Image Reference string
 */
export function parseMdImage(imageId: string, imageRef: string) {
    // fix relative path reference (to use in VuePress and does not harm other applications)
    let preRef = './';

    if (VALID_IMG_REFS.find(validImgRef => imageRef.startsWith(validImgRef))) {
        // there is already a valid reference
        preRef = ''
    }

    return `![${imageId}](${preRef}${imageRef})`;
}

/**
 * Get markdown link reference from content
 * @param mdContent content string
 * @returns reference of the link (relative or URL)
 */
export function getMdContentLinkRef(mdContent: string) {
    return extractData(mdContent, '(', ')');
}

/**
 * Get markdown link name from content
 * @param mdContent content string
 * @returns name of the link
 */
 export function getMdContentLinkName(mdContent: string) {
    return extractData(mdContent, '[', ']');
}